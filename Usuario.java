package Fumobet.demo.entidades;

public class Usuario {

    private int id;
    private String consentimento;
    private int dataNascimento;
    private String nome;
    private int cpf;
    private String email;
    private String perfil;
    private String role;
    private int senha;
    private int telefone;
    private String estado;
    private String municipio;
    private int cep;
    private String bairro;
    private String logradouro;
    private int numero;
    private String complemento;
}
